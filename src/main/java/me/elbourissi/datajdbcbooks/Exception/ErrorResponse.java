package me.elbourissi.datajdbcbooks.Exception;

import lombok.*;

import java.time.LocalDateTime;
import java.util.List;
@NoArgsConstructor @Getter @Setter
public class ErrorResponse {
    private Boolean success;
    private String message;
    private LocalDateTime dateTime;
    private List<String> details;

    public ErrorResponse(String message, List<String> details) {
        this.message = message;
        this.details = details;
        this.success = Boolean.FALSE;
        this.dateTime = LocalDateTime.now();
    }
}
