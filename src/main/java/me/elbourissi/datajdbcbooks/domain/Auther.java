package me.elbourissi.datajdbcbooks.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.*;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotNull;
import lombok.*;
import me.elbourissi.datajdbcbooks.audit.Auditable;
import me.elbourissi.datajdbcbooks.validitor.IpAddress;

import java.util.ArrayList;
import java.util.List;


@Table(name = "AUTHER")
@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Auther extends Auditable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotNull(message = "Name is required")
    private String name;
    @IpAddress(message = "Ip adress should be valid")
    private String ipAdress;
    @Email(message = "Email should be valid")
    private String email;
    @OneToMany(mappedBy = "auther", cascade = CascadeType.ALL)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    //@JsonManagedReference
    private List<Book> books = new ArrayList<>();
}
