package me.elbourissi.datajdbcbooks.domain;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.*;
import me.elbourissi.datajdbcbooks.audit.Auditable;

@Table(name = "BOOK")
@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Book extends Auditable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotNull(message = "Title is required")
    private String title;
    private Double price;
    @ManyToOne
    @JoinColumn(name = "auther_id")
    private Auther auther;
}
