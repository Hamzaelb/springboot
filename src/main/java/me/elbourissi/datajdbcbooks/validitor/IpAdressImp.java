package me.elbourissi.datajdbcbooks.validitor;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class IpAdressImp implements ConstraintValidator<IpAddress,String> {
    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        if (s == null || s.isEmpty()) {
            return true;
        }
        Pattern pattern = Pattern.compile("^(?:[0-9]{1,3}\\.){3}[0-9]{1,3}$");
        Matcher matcher = pattern.matcher(s);
        try {
            if (matcher.matches()) {
                String[] parts = s.split("\\.");
                for (int i = 0; i < parts.length; i++) {
                    int n = Integer.parseInt(parts[i]);
                    if (n < 0 || n > 255) {
                        return false;
                    }
                }
                return true;
            } else {
                return false;
            }
        } catch (NumberFormatException e) {
            return false;
        }
    }
}
