package me.elbourissi.datajdbcbooks.validitor;

import jakarta.validation.Constraint;

import java.lang.annotation.*;

@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = {IpAdressImp.class})
@Documented
public @interface IpAddress {
    String message() default "Invalid IP address";
    Class<?>[] groups() default {};
    Class<? extends jakarta.validation.Payload>[] payload() default {};
}
