package me.elbourissi.datajdbcbooks.mapper;

import me.elbourissi.datajdbcbooks.domain.Auther;
import me.elbourissi.datajdbcbooks.dto.AutherDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import org.springframework.stereotype.Component;

@Mapper(componentModel = "spring")
@Component
public interface AutherMapper {

    // map from entity to dto
    AutherDTO toDto(Auther auther);
    // map from dto to entity
    Auther toEntity(AutherDTO autherDTO);

}
