package me.elbourissi.datajdbcbooks.mapper;

import me.elbourissi.datajdbcbooks.domain.Book;
import me.elbourissi.datajdbcbooks.dto.BookDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper
public interface BookMapper {
    // map from entity to dto
    @Mapping(source = "auther.name", target = "autherName")
    @Mapping(source = "auther.email", target = "autherEmail")
    BookDTO toDto(Book book);
    // map from dto to entity
    @Mapping(source = "autherName", target = "auther.name")
    @Mapping(source = "autherEmail", target = "auther.email")
    Book toEntity(BookDTO bookDTO);
}
