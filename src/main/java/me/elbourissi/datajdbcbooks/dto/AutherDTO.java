package me.elbourissi.datajdbcbooks.dto;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotNull;
import lombok.*;
import me.elbourissi.datajdbcbooks.validitor.IpAddress;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AutherDTO {
    private Long id;
    @NotNull(message = "Name is required")
    private String name;
    @IpAddress(message = "Ip adress should be valid")
    private String ipAdress;
    @Email(message = "Email should be valid")
    private String email;

}
