package me.elbourissi.datajdbcbooks.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ConsumingAPIDTO {
    private Long id;
    private Long userId;
    private String title;
    private String body;
}
