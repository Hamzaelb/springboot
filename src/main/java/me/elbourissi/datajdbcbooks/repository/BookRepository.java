package me.elbourissi.datajdbcbooks.repository;

import me.elbourissi.datajdbcbooks.domain.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

public interface BookRepository extends JpaRepository<Book, Long> {
    @Transactional
    @Modifying
    @Query("delete from Book b where b.auther.id = :id")
    int deleteByAuther(Long id);
}
