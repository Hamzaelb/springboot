package me.elbourissi.datajdbcbooks.repository.specification;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import me.elbourissi.datajdbcbooks.domain.Auther;
import org.springframework.data.jpa.domain.Specification;

public class AutherSpecification implements Specification<Auther> {
    private String authName;

    public AutherSpecification(String authName) {
        this.authName = authName;
    }

    @Override
    public Predicate toPredicate(Root<Auther> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
        if(authName != null || !authName.isEmpty()){
            return criteriaBuilder.like(root.get("name"), authName);
        }
       return null;
    }
}
