package me.elbourissi.datajdbcbooks.repository;

import me.elbourissi.datajdbcbooks.domain.Auther;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface AutherRepository extends JpaRepository<Auther, Long>, JpaSpecificationExecutor<Auther> {
}
