package me.elbourissi.datajdbcbooks.service;

import me.elbourissi.datajdbcbooks.Exception.IdNotFoundException;
import me.elbourissi.datajdbcbooks.domain.Auther;
import me.elbourissi.datajdbcbooks.repository.AutherRepository;
import me.elbourissi.datajdbcbooks.repository.specification.AutherSpecification;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AutherService {
    private final AutherRepository autherRepository;

    public AutherService(AutherRepository autherRepository) {
        this.autherRepository = autherRepository;
    }

    public Auther findAutherById(Long id) {
        return autherRepository.findById(id).orElseThrow(() -> new IdNotFoundException("Auther Id " + id + " not found"));
    }
    @Cacheable(value = "autherCache", key = "#root.methodName")
    public List<Auther> findAll() {
        return autherRepository.findAll();
    }

    public Auther saveAuther(Auther auther) {
        if (auther.getId() != null)
            throw new RuntimeException("Auther id must be null");
        return autherRepository.save(auther);
    }

    public Auther updateAuther(Auther auther) {
        Auther autherfinded = findAutherById(auther.getId());
        autherfinded.setName(auther.getName());
        return autherRepository.save(autherfinded);
    }

    public void deleteAutherById(Long id) {
        autherRepository.deleteById(id);
    }
    public List<Auther> findByAutherSpecification(String authName) {
        AutherSpecification autherSpecification = new AutherSpecification(authName);
        return autherRepository.findAll(autherSpecification);
    }

}
