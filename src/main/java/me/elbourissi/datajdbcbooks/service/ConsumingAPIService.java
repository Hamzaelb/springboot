package me.elbourissi.datajdbcbooks.service;

import io.micrometer.core.instrument.MultiGauge;
import jakarta.persistence.Table;
import me.elbourissi.datajdbcbooks.dto.ConsumingAPIDTO;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.awt.image.BufferedImage;
import java.util.List;

@Service

public class ConsumingAPIService {
    private static final String API_URL = "https://jsonplaceholder.typicode.com/posts";

    public ConsumingAPIDTO getPostById(Long id) {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<ConsumingAPIDTO> responseEntity = restTemplate.getForEntity(API_URL + "/" + id, ConsumingAPIDTO.class);
        return responseEntity.getBody();
    }

    public List<ConsumingAPIDTO> getAllPosts() {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<List> responseEntity = restTemplate.getForEntity(API_URL, List.class);
        return responseEntity.getBody();
    }

    public ConsumingAPIDTO addPost(ConsumingAPIDTO dto) {
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<ConsumingAPIDTO> request = new HttpEntity<>(dto);
        ResponseEntity<ConsumingAPIDTO> postForEntity = restTemplate.postForEntity(API_URL, request, ConsumingAPIDTO.class);
        return postForEntity.getBody();
    }

    public void updatePost(ConsumingAPIDTO dto) {
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<ConsumingAPIDTO> request = new HttpEntity<>(dto);
        restTemplate.put(API_URL + "/" + dto.getId(), request);
    }

    public void deletePost(Long id) {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.delete(API_URL + "/" + id);
    }

}
