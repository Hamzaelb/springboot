package me.elbourissi.datajdbcbooks.service;

import com.aspose.cells.Workbook;
import com.aspose.cells.Worksheet;
import lombok.extern.slf4j.Slf4j;
import me.elbourissi.datajdbcbooks.domain.Book;
import me.elbourissi.datajdbcbooks.repository.BookRepository;
import org.springframework.stereotype.Service;

import java.time.format.DateTimeFormatter;
import java.util.List;
import java.io.ByteArrayOutputStream;
import org.springframework.core.io.ClassPathResource;

@Service
@Slf4j
public class BookService {
    private final BookRepository bookRepository;

    public BookService(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    public Book findBookById(Long id) {
        return bookRepository.findById(id).orElseThrow();
    }

    public List<Book> findAll() {
        return bookRepository.findAll();
    }

    public Book saveBook(Book book) {
        if (book.getId() != null)
            throw new RuntimeException("Auther id must be null");
        return bookRepository.save(book);
    }

    public Book updateBook(Book book) {
        Book bookfinded = findBookById(book.getId());
        bookfinded.setTitle(book.getTitle());
        bookfinded.setPrice(book.getPrice());
        return bookRepository.save(bookfinded);
    }

    public void deleteBookById(Long id) {
        bookRepository.deleteById(id);
    }
    public int deleteByAuther(Long id) {
        return bookRepository.deleteByAuther(id);
    }

    public byte[] exportBooks() throws Exception {
        List<Book> books = bookRepository.findAll();
        Workbook workbook = new Workbook();
        Worksheet sheet = workbook.getWorksheets().get(0);
        ByteArrayOutputStream docOutStream = new ByteArrayOutputStream();
        sheet.setName("books");
        sheet.copy(generateBooksExcel(books));
        workbook.save(docOutStream, com.aspose.cells.SaveFormat.XLSX);
        return docOutStream.toByteArray();
    }
    private Worksheet generateBooksExcel(List<Book> books) {
        try {
            ClassPathResource pathResource = new ClassPathResource("templates/Book_Export.xlsx");
            Workbook book = new Workbook(pathResource.getInputStream());
            Worksheet sheet = book.getWorksheets().get(0);
            int row = 2;
            for (Book b : books) {
                sheet.getCells().get("A" + row).setValue(b.getId());
                sheet.getCells().get("B" + row).setValue(b.getCreatedBy());
                sheet.getCells().get("C" + row).setValue(b.getCreatedDate().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
                sheet.getCells().get("D" + row).setValue(b.getPrice());
                sheet.getCells().get("E" + row).setValue(b.getTitle());
//                List<Role> roles = user.getRoles();
//                String rolesStr = "";
//                for (Role role : roles) {
//                    rolesStr += role.getLabel() + ", ";
//                }
//                if (!rolesStr.isEmpty()) {
//                    rolesStr = rolesStr.substring(0, rolesStr.length() - 2);
//                }
//                sheet.getCells().get("E" + row).setValue(rolesStr);
//                sheet.getCells().get("F" + row).setValue(user.getTimeout());
                row++;
            }
            return sheet;
        } catch (Exception e) {
            log.error("Error when creating user excel file", e);
        }
        return null;
    }
}
