package me.elbourissi.datajdbcbooks.config;

import me.elbourissi.datajdbcbooks.domain.Auther;
import me.elbourissi.datajdbcbooks.domain.Book;
import me.elbourissi.datajdbcbooks.service.AutherService;
import me.elbourissi.datajdbcbooks.service.BookService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.stream.Stream;

@Component
public class StartUpConfig implements CommandLineRunner {
    private final AutherService autherService;
    private final BookService bookService;

    public StartUpConfig(AutherService autherService, BookService bookService) {
        this.autherService = autherService;
        this.bookService = bookService;
    }

    @Override
    public void run(String... args) throws Exception {
        Stream.of("Hamza", "Mohamed", "Ali", "Yassine", "Youssef")
                .forEach(name -> {
                    Auther auther = new Auther();
                    auther.setName(name);
                    auther.setEmail(name + "@gmail.com");
                    auther.setIpAdress("192.168.1." + (int) (Math.random() * 255));
                    autherService.saveAuther(auther);
                });
        Stream.of("Java", "Spring", "Hibernate", "JPA", "DataJDBC")
                .forEach(name -> {
                    Book book = new Book();
                    book.setTitle(name);
                    book.setPrice(Math.random() * 100);
                    book.setAuther(Math.random()> 0.5 ? autherService.findAutherById(1L) : autherService.findAutherById(2L) );
                    bookService.saveBook(book);
                });
    }
}
