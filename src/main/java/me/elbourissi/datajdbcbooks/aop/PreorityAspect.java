//package me.elbourissi.datajdbcbooks.aop;
//
//import lombok.extern.slf4j.Slf4j;
//import org.apache.commons.lang3.StringUtils;
//import org.aspectj.lang.JoinPoint;
//import org.aspectj.lang.ProceedingJoinPoint;
//import org.aspectj.lang.annotation.Around;
//import org.aspectj.lang.annotation.Aspect;
//import org.aspectj.lang.annotation.Before;
//import org.aspectj.lang.annotation.Pointcut;
//import org.springframework.core.annotation.Order;
//import org.springframework.stereotype.Component;
//
//@Aspect
//@Slf4j
//@Order(0)
//@Component
//public class PreorityAspect {
//    @Around(value = "execution(*  me.elbourissi.datajdbcbooks.service..*(..))")
//    public Object logTime(ProceedingJoinPoint joinPoint) throws Throwable {
//        long startTime = System.currentTimeMillis();
//        StringBuilder sb = new StringBuilder("KPI:");
//        sb.append("[").append(joinPoint.getKind()).append("]\tfor: ").append(joinPoint.getSignature())
//                .append("\twithArgs: ").append("(").append(StringUtils.join(joinPoint.getArgs(), ",")).append("]");
//        sb.append("\ttook: ");
//        Object returnValue = joinPoint.proceed();
//        log.info(sb.append(System.currentTimeMillis() - startTime).append(" ms.").toString());
//        return returnValue;
//
//    }
//
//}
