//package me.elbourissi.datajdbcbooks.aop;
//
//import lombok.extern.slf4j.Slf4j;
//import org.aspectj.lang.JoinPoint;
//import org.aspectj.lang.annotation.Aspect;
//import org.aspectj.lang.annotation.Before;
//import org.aspectj.lang.annotation.Pointcut;
//import org.springframework.core.annotation.Order;
//import org.springframework.stereotype.Component;
//
//@Aspect
//@Slf4j
//@Order(1)
//@Component
//public class LogTimeAspect {
//    @Pointcut(value = "execution(*  me.elbourissi.datajdbcbooks.repository..*(..))")
//    public void forRepository() {
//
//    }
//
//    @Pointcut(value = "execution(*  me.elbourissi.datajdbcbooks.controller..*(..))")
//    public void forController() {
//
//    }
//
//    @Pointcut(value = "forRepository() || forController()")
//    public void forALL() {
//
//    }
//
//    @Before(value = "forALL()")
//    public void beforeMethode(JoinPoint joinPoint) {
//        String methodName = joinPoint.getSignature().getName();
//        log.info("Before method: " + methodName);
//        Object[] args = joinPoint.getArgs();
//        for (Object arg : args) {
//            log.info("Argument: " + arg);
//        }
//    }
//}
