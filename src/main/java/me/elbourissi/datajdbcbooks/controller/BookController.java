package me.elbourissi.datajdbcbooks.controller;

import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.Valid;
import me.elbourissi.datajdbcbooks.domain.Book;
import me.elbourissi.datajdbcbooks.dto.BookDTO;
import me.elbourissi.datajdbcbooks.service.BookService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/book")
public class BookController {
    private final BookService bookService;

    public BookController(BookService bookService) {
        this.bookService = bookService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> findById(@PathVariable Long id) {
        Book book = bookService.findBookById(id);
        BookDTO bookDTO = new BookDTO();
        bookDTO.setId(book.getId());
        bookDTO.setTitle(book.getTitle());
        bookDTO.setPrice(book.getPrice());
        return ResponseEntity.ok(bookDTO);
    }

    @GetMapping("/all")
    public ResponseEntity<?> findAll() {
        return ResponseEntity.ok(bookService.findAll());
    }

    @PostMapping("/save")
    public ResponseEntity<?> saveBook(@RequestBody @Valid Book book) {
        return ResponseEntity.ok(bookService.saveBook(book));
    }

    @PutMapping("/update")
    public ResponseEntity<?> updateBook(@RequestBody @Valid Book book) {
        return ResponseEntity.ok(bookService.updateBook(book));
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteById(@PathVariable Long id) {
        bookService.deleteBookById(id);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/deleteByAuther/{id}")
    public ResponseEntity<?> deleteByAuther(@PathVariable Long id) {
        return ResponseEntity.ok(bookService.deleteByAuther(id));
    }

    @GetMapping("/export-to-excel")
    public ResponseEntity<byte[]> exportToExcel(HttpServletResponse response) throws Exception {
        byte[] exportUsers = bookService.exportBooks();
        HttpHeaders header = new HttpHeaders();
        header.set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=Books_Information.xlsx");
        header.setContentType(new MediaType("application", "xlsx"));
        header.setContentLength(exportUsers.length);
        return new ResponseEntity<>(exportUsers, header, HttpStatus.OK);
    }
}
