package me.elbourissi.datajdbcbooks.controller;

import jakarta.validation.Valid;
import me.elbourissi.datajdbcbooks.domain.Auther;
import me.elbourissi.datajdbcbooks.dto.AutherDTO;
import me.elbourissi.datajdbcbooks.mapper.AutherMapper;
import me.elbourissi.datajdbcbooks.service.AutherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/auther")
public class AutherController {
    private final AutherService autherService;
    @Autowired
    private  AutherMapper autherMapper;

    public AutherController(AutherService autherService) {
        this.autherService = autherService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> findAutherById(@PathVariable Long id) {
        Auther autherById = autherService.findAutherById(id);
        AutherDTO dto = autherMapper.toDto(autherById);
        return ResponseEntity.ok(dto);
    }

    @GetMapping("/all")
    public ResponseEntity<?> findAll() {
        return ResponseEntity.ok(autherService.findAll());
    }

    @PostMapping("/save")
    public ResponseEntity<?> saveAuther(@RequestBody @Valid AutherDTO dto) {
        Auther auther = autherMapper.toEntity(dto);
        Auther entity = autherService.saveAuther(auther);
        AutherDTO autherDTO = autherMapper.toDto(entity);
        return ResponseEntity.ok(autherDTO);
    }

    @PutMapping("/update")
    public ResponseEntity<?> updateAuther(@RequestBody @Valid AutherDTO dto) {
        Auther auther = autherMapper.toEntity(dto);
        Auther entity = autherService.updateAuther(auther);
        AutherDTO autherDTO = autherMapper.toDto(entity);
        return ResponseEntity.ok(autherDTO);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteAutherById(@PathVariable Long id) {
        autherService.deleteAutherById(id);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/spec")
    public ResponseEntity<?> findByAutherSpecification(@RequestParam String authName) {
        return ResponseEntity.ok(autherService.findByAutherSpecification(authName));
    }
}
