package me.elbourissi.datajdbcbooks.controller;

import me.elbourissi.datajdbcbooks.dto.ConsumingAPIDTO;
import me.elbourissi.datajdbcbooks.service.ConsumingAPIService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/posts")
public class ConsumingAPIController {
    private final ConsumingAPIService consumingAPIService;

    public ConsumingAPIController(ConsumingAPIService consumingAPIService) {
        this.consumingAPIService = consumingAPIService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getPostById(@PathVariable Long id) {
        return ResponseEntity.ok(consumingAPIService.getPostById(id));
    }
    @GetMapping("/all")
    public ResponseEntity<?> getAllPosts() {
        return ResponseEntity.ok(consumingAPIService.getAllPosts());
    }
    @PostMapping("/add")
    public ResponseEntity<?> addPost(@RequestBody ConsumingAPIDTO dto) {
        return ResponseEntity.ok(consumingAPIService.addPost(dto));
    }
}
