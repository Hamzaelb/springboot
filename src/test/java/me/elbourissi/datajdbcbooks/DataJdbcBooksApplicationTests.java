package me.elbourissi.datajdbcbooks;

import me.elbourissi.datajdbcbooks.domain.Auther;
import me.elbourissi.datajdbcbooks.service.AutherService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class DataJdbcBooksApplicationTests {
    @Autowired
    AutherService autherService;

    @Test
    void findAutherById() {
        Auther auther = autherService.findAutherById(1L);
        assertEquals("Hamza", auther.getName());
    }

}
