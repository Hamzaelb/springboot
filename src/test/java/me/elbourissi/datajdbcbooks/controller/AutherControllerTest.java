package me.elbourissi.datajdbcbooks.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import me.elbourissi.datajdbcbooks.domain.Auther;
import me.elbourissi.datajdbcbooks.dto.AutherDTO;
import me.elbourissi.datajdbcbooks.service.AutherService;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class AutherControllerTest {
    @Autowired
    TestRestTemplate restTemplate;
    @Autowired
    MockMvc mockMvc;
    @MockBean
    AutherService autherService;
    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void testSaveAuther() {
        AutherDTO autherDTO = new AutherDTO();
        autherDTO.setName("test");
        autherDTO.setEmail("test@gmail.com");
        ResponseEntity<AutherDTO> responseEntity = restTemplate.postForEntity("/auther/save", autherDTO, AutherDTO.class);
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);

    }

    @Test
    public void testSaveAutherWithMockMvc() throws Exception {
        Auther auther = new Auther(null, "hamza", "192.168.2.5", "hamza@hotmail.com", null);
        AutherDTO autherDTO = new AutherDTO(null, "hamza", "192.168.2.5", "hamza@hotmail.com");
        Mockito.when(autherService.saveAuther(Mockito.any(Auther.class)))
                .thenReturn(auther);
        mockMvc.perform(post("/auther/save")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(autherDTO)))
                .andExpect(status().isOk());
    }
}
