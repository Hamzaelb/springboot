FROM openjdk:openjdk-19
EXPOSE 8080
ADD target/
ENTRYPOINT ["java", "-jar", "app.jar"]